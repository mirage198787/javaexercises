public class Account {

    String description;
    String IBAN;
    Double balance;

    public void Account(String description,String IBAN,Double balance) {
        this.setDescription(description);
        this.setIBAN(IBAN);
        this.setBalance(balance);
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIBAN() {
        return IBAN;
    }

    public Double getBalance() {
        return balance;
    }

    public String getDescription() {
        return description;
    }
}